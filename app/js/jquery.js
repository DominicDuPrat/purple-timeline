$(document).ready(function(){
    
    $('.grid').masonry({
      // options
      itemSelector: '.grid-item',
      columnWidth: '.grid-sizer',
      percentPosition: true
    });
    
   function fadeOutBoxes() {
      setInterval(function(){
        var i = Math.floor(Math.random() * 10) + 1;
            $('.grid-item img').animate({
                opacity: 1
            }, 500);
            $('.grid-item-' + i + ' img').animate({
                opacity: 0
            }, 500);
        }, 3000); 
   }
   //fadeOutBoxes(); 
    
    function headerHeight() {
        var headerHeight = $('.header-site').outerHeight();
        $('.sectionGrid').css("margin-top", headerHeight);
    }
    
    headerHeight();
    
    $(window).on('resize', function(){
          headerHeight();
    });
    
});